# softhouse-mailer

It's deployed on [opeNode.io](https://www.openode.io/)

This application serves as mailer server for softhouse company. We use it to send mail to our potential new customers. For now, we are just starting our business, so we want to use free hosting for our app to spare some money in the beginning.
