const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = 80;
const { sendMail, receivedMessage } = require('./test-email');

app.use(bodyParser.json());

app.post('/message/submit', (req, res) => {
  const { name, email, message } = req.body;
  console.log(name, email, message);
  receivedMessage(name, email, message).then(() => res.sendStatus(200));
});

app.post('/email/send', (req, res) => {
  const { from, text } = req.body;
  console.log(from, text);
  sendMail(from, text).then(() => res.sendStatus(200));
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
