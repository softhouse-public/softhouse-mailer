const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
console.log(process.env.SENDGRID_API_KEY);

function sendMail(from, text) {
  console.log(sgMail);
  const msg = {
    to: 'jan@softhouse.sk',
    from,
    subject: 'Filled form',
    text,
    // html: '<strong>and easy to do anywhere, even with Node.js</strong>',
  };
  return sgMail.send(msg);
}

function receivedMessage(name, email, message) {
  const msg = {
    to: ['jan@softhouse.sk', 'richard@softhouse.sk'],
    from: 'jan@softhouse.sk',
    subject: 'Contact form',
    templateId: 'd-80e26f3d256f47a691c2769f55fae594',
    dynamic_template_data: {
      name,
      email,
      message,
    },
  };

  return sgMail.send(msg);
}

module.exports = { receivedMessage, sendMail };
